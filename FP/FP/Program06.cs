﻿//using System;
//using System.Linq;
//using System.Collections.Generic;

//namespace FP
//{
   
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            var days = Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>();

//            Console.Write("Days of the week: ");
//            foreach (DayOfWeek value in days)
//                Console.Write($"{ value} ");

//            IEnumerable<DayOfWeek> daysStartingWith(string pattern) => days.Where(d => d.ToString().StartsWith(pattern));

//            var result = daysStartingWith("S");

//            Console.Write("\nDays starting with S: ");
//            foreach (DayOfWeek value in result)
//                Console.Write($"{value} ");
//        }
//    }
//}