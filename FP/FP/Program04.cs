﻿//using System;

//namespace FP
//{
//    public class Circle
//    {
//        public Circle(double radius) => Radius = radius;

//        public double Radius { get; }

//        public double Circumference => Math.PI * 2 * Radius;

//        public double Area
//        {
//            get
//            {
//                double Square(double d) => d * d;
//                return Math.PI * Square(Radius);
//            }
//        }

//        public (double Circumference, double Area) Stats => (Circumference, Area);

//    }

//    class Program
//    {
//        static void Main(string[] args)
//        {
//            Circle circle = new Circle(5);
//            Console.WriteLine($"Radius: {circle.Radius} " + $"\nCircumference: {circle.Circumference}" + $"\nArea: {circle.Area}" + $"\nStats: {circle.Stats}");
//        }
//    }
//}