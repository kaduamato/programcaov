﻿//using System;
//using System.Linq;
//using System.Collections.Generic;

//namespace FP
//{
//    public static class Extensions
//    {
//        public static Func<T2, T1, TR> SwapArgs<T1, T2, TR>(this Func<T1, T2, TR> f) => (t1, t2) => f(t2, t1);
//        public static string Roberto(this int n) => $"Roberto: {n}";
//    }
   
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            Func<int, int, int> div = (x, y) => x / y;
//            Func<int, int, int> divSwapped = div.SwapArgs();
            
//            int n1 = 10;
//            int n2 = 5;
//            Console.WriteLine($"{n1}/{n2} = {div(n1, n2)}");
//            Console.WriteLine($"{n2}/{n1} = {divSwapped(n1, n2)}");
            
//        }
//    }
//}