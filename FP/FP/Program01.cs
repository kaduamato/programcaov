﻿#if __DONT_REMOVE__
using System;
using System.Collections.Generic;
using System.Linq;

namespace FP
{
    class Program
    {
        static void Main(string[] args)
        {
            Func<int, int> square = x => x * x;
            IEnumerable<int> values = Enumerable.Range(0, 5);
            IEnumerable<int> squaredValues = values.Select(square);

            Console.Write("values: ");
            foreach (int value in values)
                Console.Write($"{value} ");
            Console.Write("\nsquere: ");
            foreach (int value in squaredValues)
                Console.Write($"{value} ");
        
        }
    }
}
#endif