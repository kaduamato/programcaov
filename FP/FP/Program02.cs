﻿#if __REMOVE__
using System;
using System.Linq;

namespace FP
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] values = { 3, 1, 0, 2 };
            Func<int, bool> isNotZero = x => x != 0;

            var sorted = values.OrderBy(x => x);
            var filtered = values.Where(isNotZero);

            Console.Write("values: ");
            foreach (int value in values)
                Console.Write($"{value} ");
            Console.Write("\nsorted: ");
            foreach (int value in sorted)
                Console.Write($"{value} ");
            Console.Write("\nnon-zero: ");
            foreach (int value in filtered)
                Console.Write($"{value} ");
        }
    }
}
#endif