﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace FP
{
    class Program
    {
        static void Main(string[] args)
        {
            var values = Enumerable.Range(1, 10);
            var filterMod2 = values.Where(n => n % 2 == 0);

            Console.Write("values: ");
            foreach (int value in values)
                Console.Write($"{value} ");
            Console.Write("\nfilterMod2: ");
            foreach (int value in filterMod2)
                Console.Write($"{value} ");

            Func<int, bool> isMod(int num) => n => n % num == 0;

            var filterMod3 = values.Where(isMod(3));
            Console.Write("\nfilterMod3: ");
            foreach (int value in filterMod3)
                Console.Write($"{value} ");

        }
    }
}