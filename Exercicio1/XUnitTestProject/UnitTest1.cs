using System;
using Xunit;
using xUnitTest;

namespace XUnitTestProject
{
    public class UnitTest1
    {
        private Program m_Calculator;
        int n1 = 10, n2 = 20;
        float n3 = 10.0f, n4 = 20.0f;


        public UnitTest1()
        {
            m_Calculator = new Program();
        }

        [Fact]
        public void TestSum()
        {
            var result = m_Calculator.Sum(n1, n2);

            Assert.Equal(30, result);
        }
        
        [Fact]
        public void TestDivInt()
        {
            var result = m_Calculator.Div(n1,n2);

            Assert.Equal(0, result);
        }
        
        [Fact]
        public void TestDivFloat()
        {
            var result = m_Calculator.Div(n3, n4);

            Assert.Equal(0.5f, result);
        }

    }
}
