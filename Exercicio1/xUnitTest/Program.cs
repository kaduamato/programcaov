﻿using System;

namespace xUnitTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public int Sum(int a, int b)
        {
            return a + b;
        }

        public int Div(int a, int b)
        {
            return a / b;
        }

        public float Div(float a, float b)
        {
            return a / b;
        }

    }
}
