﻿using System;

namespace Ex4
{
    public static class FP
    {
        public static None None => None.Value;

        public static Option<T> Some<T>(T value) => new Some<T>(value);
    }

    public struct None
    {
        public static readonly None Value = new None();
    }

    public struct Some<T>
    {
        public T Value { get; }
        public Some(T value)
        {
            if (value == null) throw new ArgumentNullException();
            Value = value;
        }
    }

    public struct Option<T>
    {
        readonly T Value;
        readonly bool IsSome;

        private Option(T value)
        {
            Value = value;
            IsSome = true;
        }

        public static implicit operator Option<T>(None _) => new Option<T>();

        public static implicit operator Option<T>(Some<T> some) => new Option<T>(some.Value);

        public static implicit operator Option<T>(T value) => value == null ? FP.None : FP.Some(value);

        public TResult Match<TResult>(Func<TResult> None, Func<T, TResult> Some) => IsSome ? Some(Value) : None();

        public override string ToString() => IsSome ? $"{Value}" : "Option.None";
    }

    static class Program
    {

        public static Option<T> EnumCheck<T>(this String s) where T : struct
        {
            T output;
            bool canBeEnum = Enum.TryParse(s, out output);

            Func<bool, Option<T>> CheckReturn = result => result ? FP.Some(output) : FP.None;

            return CheckReturn(canBeEnum);
        }
        public static Option<int> IntToString(this String s)
        {
            int output;
            bool canBeInt = int.TryParse(s, out output);

            Func<bool, Option<int>> CheckReturn = result => result ? FP.Some(output) : FP.None;

            return CheckReturn(canBeInt);
        }

        static Func<Option<string>, string> OpStrChecker = str => str.ToString() == "Option.None" ? "Hello, World" : $"Hello, {str}";

        enum weekDays { Sunday, Monday }

        static void Main(string[] args)
        {
            string one = "2000";
            string two = "Sunday";
            string three = "Robertoday";

            Console.WriteLine($"Two string is in Enum: {two.EnumCheck<weekDays>()}");
            Console.WriteLine($"Three string is in Enum: {three.EnumCheck<weekDays>()}");
            Console.WriteLine($"One string can be Int: {one.IntToString()}");
            Console.WriteLine($"Three string can be Int: {three.IntToString()}");

            Option<string> myName = "Roberto";
            Option<string> myAge = FP.None;

            Console.WriteLine(OpStrChecker(myName));
            Console.WriteLine(OpStrChecker(myAge));


        }
    }
}
