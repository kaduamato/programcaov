A função Bind é uma função do Option<T> que pode ser usada para aplicar uma função ao valor dquela Option e retorna um outro Option.
A função se utiliza do Value já presente na estrutura Option<T> e executa a função que foi passada por você.
Esse função passada tem que retonar um Option.
Use essa função quando você precisa fazer um cálculo com o valor do seu Option sem que ele deixe de ser um valor encapsulado. 
Para usar essa função use a sintaxe: nomeDoSeuOption.Bind(nomeDaFunc)