﻿using System;
using System.Collections.Generic;

namespace Ex5
{
    public static class FP
    {
        public static None None => None.Value;

        public static Option<T> Some<T>(T value) => new Some<T>(value);
    }

    public struct None
    {
        public static readonly None Value = new None();
    }

    public struct Some<T>
    {
        public T Value { get; }
        public Some(T value)
        {
            if (value == null) throw new ArgumentNullException();
            Value = value;
        }
    }

    public struct Option<T>
    {
        readonly T Value;
        readonly bool IsSome;

        private Option(T value)
        {
            Value = value;
            IsSome = true;
        }

        public static implicit operator Option<T>(None _) => new Option<T>();

        public static implicit operator Option<T>(Some<T> some) => new Option<T>(some.Value);

        public static implicit operator Option<T>(T value) => value == null ? FP.None : FP.Some(value);

        public TResult Match<TResult>(Func<TResult> None, Func<T, TResult> Some) => IsSome ? Some(Value) : None();

        public Option<T1> Bind<T1>(Func<T, Option<T1>> func) => func(Value);

        public override string ToString() => IsSome ? $"{Value}" : "Option.None";
    }


    static class Program
    {
        static Func<int, Option<int>> checkEven = value => value % 2 == 0 ? FP.Some(value) : FP.None;
        static Func<int, Option<int>> checkOdd = value => value % 2 != 0 ? FP.Some(value) : FP.None;

        static void Main(string[] args)
        {
            Option<int> number = 2;
            
            Console.WriteLine(number.Bind(checkEven));
            Console.WriteLine(number.Bind(checkOdd));
        }
    }
}
