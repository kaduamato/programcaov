﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Ex3
{
    public static class Extension
    {
        public static Func<T, bool> Negate<T>(this Func<T, bool> func) => n => !func(n);

        public static string GenerateAbbreviatedName(this String str)
        {

            Func<string, string> FirstLetterToUpper = n => n.Substring(0, 1).ToUpper();
            Func<string, string> FirstLetterToUpperDot = n => n.Substring(0, 1).ToUpper() + ". ";
            Func<string, string> RestOfWordToLower = n => n.Substring(1, n.Length - 1).ToLower();

            string[] splitString = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string[] stringWhitoutLast = splitString.Reverse().Skip(1).Reverse().ToArray();

            IEnumerable<String> letterDot = stringWhitoutLast.Select(FirstLetterToUpperDot);

            string firstNamesString = string.Join("", letterDot.ToArray());

            string firstLetter = FirstLetterToUpper(splitString[splitString.Length - 1]);
            string restOfTheWord = RestOfWordToLower(splitString[splitString.Length - 1]);

            return firstNamesString + firstLetter + restOfTheWord;

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Questão 1 - A
            Func<int, bool> checkEven = n => n % 2 == 0;
            var values = Enumerable.Range(1, 10);
            var oddList = values.Where(checkEven.Negate());

            Console.Write("\nOddList: ");
            foreach (int value in oddList)
                Console.Write($"{value} ");

            //Questão 1 - B
            List<int> numbersOutOfOrder = new List<int>() { 1, 14, 3, 65, 37, 89 };
            List<int> NumberSorter(List<int> list) 
            {
                List<int> tempList = new List<int>();

                for (int i = 0; i < list.Count; i++)
                {
                    tempList.Add(list[i]);
                }

                return quickSort(tempList); 
            }

            List<int> sorterReturn = NumberSorter(numbersOutOfOrder);

            Console.Write("\nOrderReturn: ");
            foreach (int value in sorterReturn)
                Console.Write($"{value} ");

            //Questão 2
            string name = "arnaldo cesar coelho";

            string NameAbbreviated = name.GenerateAbbreviatedName();
            Console.Write("\nNameAbbreviated: ");
            Console.WriteLine(NameAbbreviated);

            Console.ReadKey();

        }

        public static List<int> quickSort(List<int> vector)
        {
            int start = 0;
            int end = vector.Count - 1;

            quickSort(vector, start, end);

            return vector;
        }

        private static void quickSort(List<int> vector, int start, int end)
        {
            if (start < end)
            {
                int p = vector[start];
                int i = start + 1;
                int f = end;

                while (i <= f)
                {
                    if (vector[i] <= p)
                    {
                        i++;
                    }
                    else if (p < vector[f])
                    {
                        f--;
                    }
                    else
                    {
                        int change = vector[i];
                        vector[i] = vector[f];
                        vector[f] = change;
                        i++;
                        f--;
                    }
                }

                vector[start] = vector[f];
                vector[f] = p;

                quickSort(vector, start, f - 1);
                quickSort(vector, f + 1, end);
            }
        }

    }
}

