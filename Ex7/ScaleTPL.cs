﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class ScaleTPL : MonoBehaviour
{
    [SerializeField] float scale;
    private void Awake()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;

        Vector3[] vertices = mesh.vertices;

        Parallel.For(0, mesh.vertexCount, i => 
        {
            vertices[i].x *= scale;
            vertices[i].y *= scale;
            vertices[i].z *= scale;

        });

        mesh.vertices = vertices;
        mesh.RecalculateBounds();

    }
}
